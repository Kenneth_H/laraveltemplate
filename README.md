# Theme support for Laravel 5

Inspired by [yaapis/Theme](https://github.com/yaapis/Theme).

Modified to match a simplified version of the Twig based template engine in Drupal 8, with the exception being the usage of Laravel Blade instead of Twig.

Themes are stored inside default Laravel public folder.

## Installation
Require this package in your composer.json:

~~~json
"khit/laraveltheme": "dev-master"
~~~

And add the ServiceProvider to the providers array in config/app.php

~~~php
'KHIT\Theme\ThemeServiceProvider',
~~~

Publish config using artisan CLI (if you want to overwrite default config).

~~~bash
php artisan vendor:publish --tag="config"
~~~

You can register the facade in the `aliases` key of your `config/app.php` file.

~~~php
'aliases' => array(
    'Theme' => 'KHIT\Theme\Facades\Theme'
)
~~~


## Package config

~~~php
	return array(
        'path'          => base_path('public/themes'),
        'assets_path'   => base_path('public/themes'),
    );
~~~


## Theme config

~~~yaml
name: My custom theme name
base_theme: null
description: 'My custom theme.'
# version: VERSION
# core: 5.x
regions:
  header: 'Header'
  content: 'Content'
  footer: 'Footer first'
css:
    css/stylesheet-name.css: {}
js:
    js/plugins.js: {}
~~~



##Usage

### Structure

```
├── resources/
    └── themes/
        ├── default/
            ├── templates/
	        |   └── page.blade.php
	        └── default.yml

        └── admin/

    ├── views/
    |   ├── emails/
    |   |   └── notify.blade.php
    |   └── hello.blade.php
    |
    └── lang/

├── public/
    └── themes/
		└── default/
			├── css/
			|	└── styles.css
			└── images/
                └── icon.png
```

### Create theme with artisan CLI

The first time you have to create theme "default" structure, using the artisan command:

~~~bash
php artisan theme:create default
~~~

To delete an existing theme, use the command:

~~~bash
php artisan theme:destroy default
~~~

###Init theme

~~~php
Theme::init($name)
~~~

This will add to views find path:
* public/themes/{$name}
* public/themes/{$name}/templates

### Making view

~~~php
View::make('hello');
View::make('emails.notify');
~~~

### Assets
Assets can be nested too.
Asset url can be automatically with version.

~~~css
<link rel="stylesheet" href="{{ Theme::asset('css/styles.css', null, true) }}"/>
<link rel="stylesheet" href="{{ Theme::asset('css/ie.css', null, 'v1') }}"/>
~~~

The first one will get version from filemtime, the second one - from params


###Blade templates

```
	@extends('layouts.master')

	@include('partials.header')

	@section('content')

	    <section id="main">
	        <h1>HOME</h1>
	    </section>
	@stop

	@include('partials.footer')

```

###Fallback capability

You still able to use default `View::make('emails.notify')` whitch stored outside the themes directory